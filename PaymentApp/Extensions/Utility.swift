import Foundation
import UIKit

extension Bundle {
    
    // Retrieves string from localized string file, Localizable.strings
    class func localizedStringForKey(_ key: String) -> String{
        return Bundle.main.localizedString(forKey: key, value: nil, table: nil)
    }
}

extension UIColor {
    
    class func mainColor () -> UIColor
    {
        return #colorLiteral(red: 0, green: 0.6196078431, blue: 0.8901960784, alpha: 1)
    }
}

extension UIButton {
    
    func enableButton() {
        self.isEnabled = true
        self.alpha = 1
    }
    
    func disableButton() {
        self.isEnabled = false
        self.alpha = 0.7
    }
    
}

extension UITextField {
    
    func underlined(){
        self.borderStyle = UITextBorderStyle.none
        let border = CALayer()
        let width = CGFloat(2)
        border.backgroundColor = UIColor.mainColor().cgColor
        border.borderColor = UIColor.mainColor().cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: width)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension UIViewController {
    
    func getController (_ idViewController : String, storyboardName : String)  -> UIViewController? {
        
        var controller: UIViewController?
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        if let identifiers = (storyboard.value(forKey: "identifierToNibNameMap") as?
            [String : String])?.values {
            if Array(identifiers).contains(idViewController) {
                controller = storyboard.instantiateViewController(withIdentifier: idViewController)

            }
        }
        return controller
    }
}

extension String {
    
    static func stringFromNumber(_ number : NSNumber, useDecimalStyle : Bool = false) -> String? {
        let numberFormatter : NumberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.numberStyle = useDecimalStyle ? .decimal : .none
        numberFormatter.groupingSeparator = (Locale.current as NSLocale).object(forKey: NSLocale.Key.groupingSeparator) as! String
        numberFormatter.groupingSize = 3
        return numberFormatter.string(from: number)
    }
    
}
