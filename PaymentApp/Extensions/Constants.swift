
import Foundation

//MARK: PublicKey
let kPublicKey : String = "444a9ef5-8a6b-429f-abdf-587639155d88"

//MARK: STORYBOARDS ID
let kPaymentStoryboard : String = "Payment"
let kIDStartPaymentVC : String = "StartPaymentVC"
let kIDPaymentMethodVC : String = "PaymentMethodVC"
let kIDBanksVC : String = "BanksVC"
let kIDPaymentFeesVC : String = "PaymentFeesVC"
