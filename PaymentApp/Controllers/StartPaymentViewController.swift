
import UIKit

class StartPaymentViewController: BaseViewController, UITextFieldDelegate {
    
    fileprivate var activeTextField : UITextField?
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var instrucctionsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Bundle.localizedStringForKey("kStartPayment")
        self.edgesForExtendedLayout = UIRectEdge()
        // Do base setup here
        setupView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapContinue(_ sender: Any) {
        
        if (amountTextField.text?.count)! > 2 {
            self.performSegue(withIdentifier: "seguePaymentMethod", sender: self)
        }
        else {
            instrucctionsLabel.textColor = UIColor.red
        }
    }
    
    //MARK: Segue prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? PaymentMethodViewController {
            let amountString = ("\(amountTextField.text!.replacingOccurrences(of: "$", with: ""))").replacingOccurrences(of: ".", with: "")
            destination.amount = amountString
        }
    }
    
    // MARK: - TextField delegate
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text != "$" {
         let amountValue = Double(("\(amountTextField.text!.replacingOccurrences(of: "$", with: ""))").replacingOccurrences(of: ".", with: ""))
        let amountText = String.stringFromNumber(NSNumber(value: amountValue!))!
       textField.text = "$" + amountText.replacingOccurrences(of: ",", with: ".")
        }
        else {
            textField.text = ""
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField = nil
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = (textField.text?.count)! + string.count - range.length
        
        if amountTextField == textField
        {
            if newLength > 2
            {
                instrucctionsLabel.textColor = UIColor.gray
                continueButton.backgroundColor = UIColor.mainColor()
            }
            else {
                continueButton.backgroundColor = UIColor.lightGray
            }
            
            return newLength > 8 ? false : true
        }
        return true
    }
    
    // MARK: - Private
    
    
    
    private func setupView() {
        view.backgroundColor = UIColor.white
        
        //Setup button
        //continueButton.disableButton()
        continueButton.backgroundColor = UIColor.lightGray
        continueButton.setTitle(Bundle.localizedStringForKey("kContinueButton"), for: .normal)
        continueButton.setTitleColor(UIColor.white, for: .normal)
        continueButton.titleLabel?.font = UIFont.bodyTitleBold()
        
        //Setup descriptionLabel
        
        descriptionLabel.backgroundColor = UIColor.white
        descriptionLabel.font = UIFont.systemFont(ofSize: 18)
        descriptionLabel.textColor = UIColor.darkGray
        descriptionLabel.textAlignment = .center
        descriptionLabel.text = Bundle.localizedStringForKey("kStartPaymentDescription")
        descriptionLabel.numberOfLines = 0
        
        //Setup amountTextField
        
        amountTextField.font = UIFont.systemFont(ofSize: 36)
        amountTextField.textAlignment = .center
        amountTextField.placeholder = "$0"
        amountTextField.underlined()
        amountTextField.keyboardType = .numberPad
        amountTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        amountTextField.becomeFirstResponder()
        
        //Setup instrucctionLabel
        
        instrucctionsLabel.backgroundColor = UIColor.white
        instrucctionsLabel.font = UIFont.systemFont(ofSize: 16)
        instrucctionsLabel.textColor = UIColor.gray
        instrucctionsLabel.textAlignment = .center
        instrucctionsLabel.text = Bundle.localizedStringForKey("kInstrucctionPayment")
        instrucctionsLabel.numberOfLines = 0
    }    
}

