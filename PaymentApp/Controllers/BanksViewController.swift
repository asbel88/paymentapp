
import UIKit

class BanksViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var aTableView: UITableView!
    
    var amount : String!
    var paymentMethod : PaymentMethod!
    
    var apiClient : APIServiceProtocol = PaymentsAPIManager.sharedInstance
    
    var banksList : [Banks] = []
    
    fileprivate var shouldRemoveScrimView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = Bundle.localizedStringForKey("kPaymentBank")
        self.edgesForExtendedLayout = UIRectEdge()
       
        // Do tableView setup here
        setupTableView()
        
        // Do base setup here
        setupView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBanks()
    }
    
    // MARK: - TableView DataSource & Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return banksList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Add the View Completed hardcoded cell at the bottom
        let cell : BankTableViewCell = aTableView.dequeueReusableCell(withIdentifier: "BankTableViewCell") as! BankTableViewCell
        cell.configureCell(withBank: banksList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = getController(kIDPaymentFeesVC, storyboardName: kPaymentStoryboard) as? PaymentFeesViewController {
            
            controller.amount = self.amount
            controller.paymentMethod = self.paymentMethod
            controller.bank = self.banksList[indexPath.row]
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK : - Private
    
    fileprivate func removeScrimIfNeeded() {
        if shouldRemoveScrimView {
            hideScrimView()
        } else {
            shouldRemoveScrimView = true
        }
    }
    
    fileprivate func setupTableView() {
        aTableView.alwaysBounceVertical = false
        aTableView.dataSource = self
        aTableView.delegate = self
        aTableView.backgroundColor = UIColor.clear
        aTableView.register(UINib.init(nibName: "BankTableViewCell", bundle: nil), forCellReuseIdentifier: "BankTableViewCell")
        
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.white
        
        //Setup descriptionLabel
        descriptionLabel.backgroundColor = UIColor.white
        descriptionLabel.font = UIFont.systemFont(ofSize: 18)
        descriptionLabel.textColor = UIColor.darkGray
        descriptionLabel.textAlignment = .center
        descriptionLabel.text = Bundle.localizedStringForKey("kPaymentBanksDescription")
        descriptionLabel.numberOfLines = 0
        
    }
    
    // MARK: - Service calls
    
    func getBanks() {
        let success : APICompletionHandler = { [weak self] data in
            self?.hideScrimView()
            if let banks = data as? [Any] {
                self?.handleBanksResponse(banks)
            }
        }
        
        let failure : APIFailureHandler = getDefaltFailureHandler(Bundle.localizedStringForKey("kActivitiesErrorTitle"), message: Bundle.localizedStringForKey("kActivitiesErrorMessage"))
        showScrimView()
        apiClient.getBanks(paymentMethod.id, completionHandler: success, failureHandler: failure)
    }
    
    //Mark: Handling response
    
    fileprivate func handleBanksResponse(_ banks : [Any]) {
        banksList.removeAll()
        for bank in banks {
            let paymentBank : Banks = Banks(dictionary: bank as! [String : Any])
            banksList.append(paymentBank)
        }
        
        aTableView.reloadData()
    }

}
