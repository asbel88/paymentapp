
import UIKit

protocol CustomAlertViewDelegate: class {
    func okButtonTapped()
}

class CustomAlertViewController: UIViewController {
    
    var amount : String!
    var paymentMethod : PaymentMethod!
    var bank : Banks!
    var installent: String!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var methodPaymentLabel: UILabel!
    @IBOutlet weak var installentsLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var okButton: UIButton!
    
    @IBOutlet weak var methodImageView: UIImageView!
    @IBOutlet weak var bankImageView: UIImageView!
    var delegate: CustomAlertViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }
    
    @IBAction func onTapOkButton(_ sender: Any) {
        delegate?.okButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK : - Private
    
    private func setupView() {
        
        //SetUp Modal View
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        //Setup okButton
        okButton.setTitle(Bundle.localizedStringForKey("koKButton"), for: .normal)
        okButton.setTitleColor(UIColor.mainColor(), for: .normal)
        okButton.titleLabel?.font = UIFont.bodyTitleBold()
        
        //Setup titleLabel
        titleLabel.font = UIFont.bodyTitleBold()
        titleLabel.textColor = UIColor.darkGray
        titleLabel.textAlignment = .center
        titleLabel.text = Bundle.localizedStringForKey("kPaymentDetails")
        titleLabel.numberOfLines = 0
        
        //Change format for amount value
        let amountValue = Double(amount)
        let amountText = String.stringFromNumber(NSNumber(value: amountValue!))!
        
        //Setup amountLabel
        amountLabel.font = UIFont.systemFont(ofSize: 14)
        amountLabel.textColor = UIColor.darkGray
        amountLabel.textAlignment = .left
        amountLabel.numberOfLines = 0
        amountLabel.text = Bundle.localizedStringForKey("kPaymentAmount") + amountText
        
        //Setup methodPaymentLabel
        methodPaymentLabel.font = UIFont.systemFont(ofSize: 14)
        methodPaymentLabel.textColor = UIColor.darkGray
        methodPaymentLabel.textAlignment = .left
        methodPaymentLabel.text = Bundle.localizedStringForKey("kPaymentMethodSelected") + paymentMethod.name
        methodPaymentLabel.numberOfLines = 0
        
        //Setup bankLabel
        bankLabel.font = UIFont.systemFont(ofSize: 14)
        bankLabel.textColor = UIColor.darkGray
        bankLabel.textAlignment = .center
        bankLabel.text = Bundle.localizedStringForKey("kPaymentBankSelected") +  bank.name
        bankLabel.numberOfLines = 0
        
        //Setup installentsLabel
        installentsLabel.font = UIFont.systemFont(ofSize: 14)
        installentsLabel.textColor = UIColor.darkGray
        installentsLabel.textAlignment = .left
        installentsLabel.text = Bundle.localizedStringForKey("kInstallentsSelected") + installent
        installentsLabel.numberOfLines = 0
        
        //Setup Images
        methodImageView.downloaded(from: paymentMethod.secureThumbnail)
        bankImageView.downloaded(from: bank.secureThumbnail)
        
    }
    
    private func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
}
