
import UIKit

class BaseViewController: UIViewController, UINavigationControllerDelegate {
    
    var scrimView : ScrimView = ScrimView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
        self.edgesForExtendedLayout = UIRectEdge()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrimView.frame = view.bounds
    }
    
    // MARK: ScrimView
    
    func showScrimView() {
        scrimView.presentInView(self.view)
    }
    
    func hideScrimView() {
        scrimView.dismissFromSuperview()
    }
    
    func hideScrimViewWithAnimations(_ animationBlock : @escaping () -> Void) {
        scrimView.dismissAnimated(animationBlock)
    }
    
//    // MARK: Toast view
//
//    func showToastWithText(_ text : String) {
//        ToastManager.sharedInstance.show(withText: text)
//    }
//
//    func showToastWithAttributedText(_ text : NSAttributedString) {
//        ToastManager.sharedInstance.show(withAttributeText: text)
//    }
//
//    func dismissToast() {
//        ToastManager.sharedInstance.close()
//    }
//
//    // MARK: CustomWebViewController messages
//
//    func openInformationSharingPreferencesPage(){
//        self.openWebViewWithTitleAndURL(Bundle.localizedSettingsStringForKey("kSettingsInformationSharingTitle"), url: Configuration.getRiskGroupURL())
//    }
//
//    func openMallWebViewWithSessionID(_ sessionID : String){
//        self.openWebViewWithTitleUrlAndSessionID(Bundle.localizedSettingsStringForKey(Bundle.localizedMallStringForKey("kMallTitle")), url: Configuration.getMallURL(), sessionID: sessionID)
//    }
//
//    func openWebViewWithTitleUrlAndSessionID(_ title:String, url:String, sessionID : String){
//
//        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
//        let webViewController = WebViewController()
//        webViewController.setupWithTitleAndURL(title, url: url)
//        let navigationController = StyledNavigationController(rootViewController: webViewController)
//        rootViewController?.present(navigationController, animated: false, completion: nil)
//    }
//
//    func openWebViewWithTitleAndURL(_ title : String, url : String){
//        let webViewController = WebViewController()
//        webViewController.setupWithTitleAndURL(title, url: url)
//        let navigationController = StyledNavigationController(rootViewController: webViewController)
//        self.present(navigationController, animated: false, completion: nil)
//    }
//
    // MARK: API Default failure handler

    func getDefaltFailureHandler(_ title : String, message : String) -> APIFailureHandler {
        return { [weak self] error in
            self?.hideScrimView()
            let alert : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Bundle.localizedStringForKey("kOkButtonTitle"), style: .cancel, handler: nil))
           /// self?.presentAlertController(alert, animated: true, completion: nil)
        }
    }
    

    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: Private methods
    
    fileprivate func setupBackButton() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

