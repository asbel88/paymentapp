
import UIKit

class PaymentFeesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var aTableView: UITableView!
    
    var amount : String!
    var paymentMethod : PaymentMethod!
    var bank : Banks!
    
    var apiClient : APIServiceProtocol = PaymentsAPIManager.sharedInstance
    
    var feesList : [PayerCost] = []
    
    fileprivate var shouldRemoveScrimView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = Bundle.localizedStringForKey("kPaymentFees")
        self.edgesForExtendedLayout = UIRectEdge()
        
        // Do tableView setup here
        setupTableView()
        
        // Do base setup here
        setupView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFees()
    }
    
    // MARK: - TableView DataSource & Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Add the View Completed hardcoded cell at the bottom
        let cell : InstallentTableViewCell = aTableView.dequeueReusableCell(withIdentifier: "InstallentTableViewCell") as! InstallentTableViewCell
        cell.configureCell(withPayerCost: feesList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertID") as! CustomAlertViewController
        //Previous setup to alertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        customAlert.delegate = self
        customAlert.amount = self.amount
        customAlert.paymentMethod = self.paymentMethod
        customAlert.bank = self.bank
        customAlert.installent = feesList[indexPath.row].recommendedMessage
        self.present(customAlert, animated: true, completion: nil)

    }
    
    // MARK : - Private
    
    fileprivate func removeScrimIfNeeded() {
        if shouldRemoveScrimView {
            hideScrimView()
        } else {
            shouldRemoveScrimView = true
        }
    }
    
    fileprivate func setupTableView() {
        aTableView.alwaysBounceVertical = false
        aTableView.dataSource = self
        aTableView.delegate = self
        aTableView.backgroundColor = UIColor.clear
         aTableView.register(UINib.init(nibName: "InstallentTableViewCell", bundle: nil), forCellReuseIdentifier: "InstallentTableViewCell")
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.white
        
        //Setup descriptionLabel
        descriptionLabel.backgroundColor = UIColor.white
        descriptionLabel.font = UIFont.systemFont(ofSize: 18)
        descriptionLabel.textColor = UIColor.darkGray
        descriptionLabel.textAlignment = .center
        descriptionLabel.text = Bundle.localizedStringForKey("kPaymentFeesDescription")
        descriptionLabel.numberOfLines = 0
        
    }
    
    // MARK: - Service calls
    
    func getFees() {
        let success : APICompletionHandler = { [weak self] data in
            self?.hideScrimView()
            if let installents = data as? [Any] {
                
                self?.handleFeesResponse(installents)
            }
        }
        
        let failure : APIFailureHandler = getDefaltFailureHandler(Bundle.localizedStringForKey("kActivitiesErrorTitle"), message: Bundle.localizedStringForKey("kActivitiesErrorMessage"))
        showScrimView()
        apiClient.getFees(paymentMethod.id, amount: amount, bank: bank.id, completionHandler: success, failureHandler: failure)
    }
    
    //Mark: Handling response
    
    fileprivate func handleFeesResponse(_ installents : [Any]) {
        feesList.removeAll()
        
        let fee : Installents = Installents(dictionary: installents.first as! [String : Any])
        feesList = fee.payerCosts
        
        aTableView.reloadData()
    }

}
extension PaymentFeesViewController: CustomAlertViewDelegate {
    
    func okButtonTapped() {

        //Call the first viewController from zero
        let mainStoryboard = UIStoryboard(name: "Payment", bundle: nil)
        let root: UINavigationController
        root = UINavigationController(rootViewController: mainStoryboard.instantiateInitialViewController()!)
        let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = root

    }
}
