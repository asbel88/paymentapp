
import UIKit

class PaymentMethodViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var aTableView: UITableView!
    
    var amount : String!
    
    var apiClient : APIServiceProtocol = PaymentsAPIManager.sharedInstance
    
    var paymentMethodsList : [PaymentMethod] = []
    
    fileprivate var shouldRemoveScrimView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = Bundle.localizedStringForKey("kPaymentMethod")
        self.edgesForExtendedLayout = UIRectEdge()
        
        // Do tableView setup here
        setupTableView()
        
        // Do base setup here
        setupView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPaymentMethods()
    }
    
    // MARK: - TableView DataSource & Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethodsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Add the View Completed hardcoded cell at the bottom
        let cell : PaymentMethodTableViewCell = aTableView.dequeueReusableCell(withIdentifier: "PaymentMethodTableViewCell") as! PaymentMethodTableViewCell
        cell.configureCell(withPaymentMethod: paymentMethodsList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = getController(kIDBanksVC, storyboardName: kPaymentStoryboard) as? BanksViewController {
            
            controller.amount = self.amount
            controller.paymentMethod = paymentMethodsList[indexPath.row]
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK : - Private
    
    fileprivate func removeScrimIfNeeded() {
        if shouldRemoveScrimView {
            hideScrimView()
        } else {
            shouldRemoveScrimView = true
        }
    }
    
    fileprivate func setupTableView() {
        aTableView.alwaysBounceVertical = false
        aTableView.dataSource = self
        aTableView.delegate = self
        aTableView.backgroundColor = UIColor.clear
        aTableView.register(UINib.init(nibName: "PaymentMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentMethodTableViewCell")
        
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.white
        
        //Setup descriptionLabel
        descriptionLabel.backgroundColor = UIColor.white
        descriptionLabel.font = UIFont.systemFont(ofSize: 18)
        descriptionLabel.textColor = UIColor.darkGray
        descriptionLabel.textAlignment = .center
        descriptionLabel.text = Bundle.localizedStringForKey("kPaymentMethodDescription")
        descriptionLabel.numberOfLines = 0
        
    }
    
    // MARK: - Service calls
    
    func getPaymentMethods() {
        let success : APICompletionHandler = { [weak self] data in
            self?.hideScrimView()
            if let methods = data as? [Any] {
                
                self?.handlePaymentMethodsResponse(methods)
            }
        }
        
        let failure : APIFailureHandler = getDefaltFailureHandler(Bundle.localizedStringForKey("kActivitiesErrorTitle"), message: Bundle.localizedStringForKey("kActivitiesErrorMessage"))
        showScrimView()
        apiClient.getPaymentMethods(success, failureHandler: failure)
    }
    
    //Mark: Handling response
    
    fileprivate func handlePaymentMethodsResponse(_ paymentMethods : [Any]) {
        
        paymentMethodsList.removeAll()
        
        for method in paymentMethods {
            let paymentMethod : PaymentMethod = PaymentMethod(dictionary: method as! [String : Any])
            paymentMethodsList.append(paymentMethod)
        }
        
        aTableView.reloadData()
    }
    
}
