import UIKit

class InstallentTableViewCell: UITableViewCell {

    @IBOutlet weak var installentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }
    
    func configureCell(withPayerCost  payerCost : PayerCost) {
        
        installentLabel.text = payerCost.recommendedMessage
    }
    
    // MARK: - Private
    
    private func setupCell() {
        self.selectionStyle = .none
        
        installentLabel.font = UIFont.body()
        installentLabel.textColor = UIColor.darkGray
        installentLabel.textAlignment = .left
    }
}
