import UIKit

class BankTableViewCell: UITableViewCell {

    @IBOutlet weak var bankImageView: UIImageView!
    @IBOutlet weak var nameBankLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }
    
    func configureCell(withBank bank : Banks) {
        
        nameBankLabel.text = bank.name
        bankImageView.downloaded(from: bank.secureThumbnail)        
    }
    
    // MARK: - Private
    
    private func setupCell() {
        self.selectionStyle = .none
        
        nameBankLabel.font = UIFont.body()
        nameBankLabel.textColor = UIColor.darkGray
        nameBankLabel.textAlignment = .left
        nameBankLabel.numberOfLines = 1
    }
}
