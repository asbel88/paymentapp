
import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var nameCardLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }

    func configureCell(withPaymentMethod payment : PaymentMethod) {
       
        nameCardLabel.text = payment.name
        cardImageView.downloaded(from: payment.secureThumbnail)
        
    }
    
    // MARK: - Private
    
    private func setupCell() {
        self.selectionStyle = .none
        
        nameCardLabel.font = UIFont.body()
        nameCardLabel.textColor = UIColor.darkGray
        nameCardLabel.textAlignment = .left
        nameCardLabel.numberOfLines = 1
    }

}
