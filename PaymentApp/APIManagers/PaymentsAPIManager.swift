import Foundation

typealias PaymentsAPIManager = BaseAPIManager

extension BaseAPIManager {
    
    func getPaymentMethods(_ completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler) {

        let queryParams = ["public_key":kPublicKey]
        
        callURLService("payment_methods", method: .Get, parameters: queryParams, success: completionHandler, failure: failureHandler)
    }
    
    func getBanks(_ paymentMethod: String, completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler) {
        
        let queryParams = ["public_key":kPublicKey, "payment_method_id": paymentMethod]
        
        callURLService("payment_methods/card_issuers", method: .Get, parameters: queryParams, success: completionHandler, failure: failureHandler)
    }

    func getFees(_ paymentMethod: String, amount: String, bank: String, completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler) {
        
        let queryParams = ["public_key":kPublicKey, "amount": amount,  "payment_method_id": paymentMethod, "issuer.id" : bank]
        
        callURLService("payment_methods/installments", method: .Get, parameters: queryParams, success: completionHandler, failure: failureHandler)
    }
}
