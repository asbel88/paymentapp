import Foundation

protocol APIServiceProtocol {
    
    func getPaymentMethods(_ completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler)
    
    func getBanks(_ paymentMethod: String, completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler)
    
    func getFees(_ paymentMethod: String, amount: String, bank: String, completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler)
}
