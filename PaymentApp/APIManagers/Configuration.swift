import Foundation

class Configuration {

    class func getScheme() -> String {
        return "https"
    }
    class func getHost() -> String {
        return "api.mercadopago.com"
    }
    
    class func getPort() -> Int? {
        return nil
    }
    
    class func getBaseURL() -> String {
        return "/v1"
    }
}
