
import UIKit

class PayerCost: NSObject {

    var discountRate : Int
    var installmentAmount : Double
    var installmentRate : Double
    var installments : Int
    var maxAllowedAmount : Int
    var minAllowedAmount : Int
    var recommendedMessage : String
    var totalAmount : Double
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(dictionary: [String:Any]){
        discountRate = dictionary.intForKey("discount_rate")
        installmentAmount = dictionary.doubleForKey("installment_amount")
        installmentRate = dictionary.doubleForKey("installment_rate")
        installments = dictionary.intForKey("installments")
        maxAllowedAmount = dictionary.intForKey("max_allowed_amount")
        minAllowedAmount = dictionary.intForKey("min_allowed_amount")
        recommendedMessage = dictionary.stringForKey("recommended_message")
        totalAmount = dictionary.doubleForKey("total_amount")
    }
}
