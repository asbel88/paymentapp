
import UIKit

class Issuer: NSObject {
    
    var id : String
    var name : String
    var secureThumbnail : String
    var thumbnail : String
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(dictionary: [String:Any]){
        id = dictionary.stringForKey("id")
        name = dictionary.stringForKey("name")
        secureThumbnail = dictionary.stringForKey("secure_thumbnail")
        thumbnail = dictionary.stringForKey("thumbnail")
    }

}
