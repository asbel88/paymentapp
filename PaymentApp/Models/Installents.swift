
import UIKit

class Installents: NSObject {

    var issuer : Issuer!
    var payerCosts : [PayerCost]!
    var paymentMethodId : String!
    var paymentTypeId : String!
    var processingMode : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(dictionary: [String : Any]) {

        paymentMethodId = dictionary.stringForKey("payment_method_id")
        paymentTypeId = dictionary.stringForKey("payment_type_id")
        processingMode = dictionary.stringForKey("processing_mode")
       
        if let issuerData  = dictionary.dictionaryForKey("issuer") as [String : Any]?
        {
            issuer = Issuer(dictionary: issuerData)
        }
        payerCosts = [PayerCost]()
        
        if let payerCostsArray  = dictionary.arrayForKey("payer_costs") as [Any]? {
            for dic in payerCostsArray{
                let value = PayerCost(dictionary: dic as! [String : Any])
                payerCosts.append(value)
            }
        }
    }
}
