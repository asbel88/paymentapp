import UIKit

class Banks: NSObject {
    var id : String
    var name : String
    var processingMode : String
    var secureThumbnail : String
    var thumbnail : String
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(dictionary: [String:Any]){
        id = dictionary.stringForKey("id")
        name = dictionary.stringForKey("name")
        processingMode = dictionary.stringForKey("processing_mode")
        secureThumbnail = dictionary.stringForKey("secure_thumbnail")
        thumbnail = dictionary.stringForKey("thumbnail")
    }
}
