import UIKit

class PaymentMethod: NSObject {

    var accreditationTime : Int
    var deferredCapture : String
    var id : String
    var maxAllowedAmount : Int
    var minAllowedAmount : Int
    var name : String
    var paymentTypeId : String
    var secureThumbnail : String
    var status : String
    var thumbnail : String
    
    init(dictionary : [String : Any]) {
        accreditationTime = dictionary.intForKey("accreditation_time")
        deferredCapture = dictionary.stringForKey("deferred_capture")
        id = dictionary.stringForKey("id")
        maxAllowedAmount = dictionary.intForKey("max_allowed_amount")
        minAllowedAmount = dictionary.intForKey("min_allowed_amount")
        name = dictionary.stringForKey("name")
        paymentTypeId = dictionary.stringForKey("payment_type_id")
        secureThumbnail = dictionary.stringForKey("secure_thumbnail")
        status = dictionary.stringForKey("status")
        thumbnail = dictionary.stringForKey("thumbnail")
        super.init()
    }
}
